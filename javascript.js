

console.log("[USOS DE CONSOLA]:");
//string
console.log("Wii!");
//number
console.log(1234);
//error
console.error("Error programat");
//taula
console.table({ a: 1, b: 2 });


console.log("[USOS DE VARIABLES]:");
// var - let - const
var nom = ""; //Function scoped (pot ser undefined linies abans a la declaració)
let nom1 = ""; //block scoped, cal tenir-la declarada inicialment al bloc actual, sempre. Per ttenir mes control casi millor oblidar els var i posar let en un projecte nou.
//const no canvia si la reassignes mes tard (com una constant de php)
const person = {
    name: "Ivan",
    edat: 36
};
person.edat = 35; //això ho deixa fer (es l'unic cas, pk ja sap l'estructura name+edat)
console.log("contingut person:", person);
console.log("typeof person:", typeof person);



console.log("[DATA TYPES]:");
//Boolean - true or false
let isTrue = true;
//Null - no value
let phone = null;
//Undefined - defined var no value
let car;
//Number - int/float autodetect from js
let numero = 3;
//String - array of characters
var cadena = "Ivan";
//Symbol - unique value
let simbol = Symbol();

//Reference types (all come back as object)
const cars = ['audi', 'BMW']; //array of type object
const cars1 = Array('audi1', 'BMW'); //array of type object
var cars2 = {
    model: 'Audi2',
    any: 2008
}; //object of type object
var today = new Date(); //object

console.log("contingut cars2:", cars2);
console.log("typeof cars2:", typeof cars2);



console.log("[TYPE CONVERSIONS]:");
let numToStr = String(2); //ok
let arrToStr = String([1, 2, 3, 4, 5, 6]); //ok
let numToStr1 = (5).toString(); //ok

console.log("contingut numToStr:", numToStr);
console.log("typeof numToStr:", typeof numToStr);
console.log("length of numToStr:", numToStr.length);

let strToNum = Number('5'); //ok
let strToNum1 = Number('la la la'); //NaN = Not a number
let arrToNum = Number([1, 2, 3, 4, 5, 6]); //NaN
let parsInt = parseInt('50.55'); // Force 50.00
let parsFloat = parseFloat('50.55'); // ok
console.log("strToNum.toFixed():", strToNum.toFixed());
console.log("strToNum.toFixed(2):", strToNum.toFixed(2));



//Arithmetic operators
console.log("[ARITHMETIC OPERATORS]:");
const num1 = 100;
const num2 = 50;
let num3 = num1 + num2;
num3 = num1 - num2;
num3 = num1 * num2;
num3 = num1 / num2;
num3 = num1 % num2; //modulus, prova canviant num1=110 per veure diff = 10
num3++; //increment num3+1
++num3; //increment +1+num3
num3--; //decrement num3-1
--num3; //decrement -1+num3
console.log("contingut num3:", num3);



console.log("[MATH OBJECTS]:");
let matVal = Math.PI;
matVal = Math.ceil(2.1); //round up
matVal = Math.floor(2.1); //round down
matVal = Math.round(2.3); //round normal
matVal = Math.sqrt(64); //arrel cuadrada
matVal = Math.abs(-3); //3
matVal = Math.pow(8, 2); //elevat a 
matVal = Math.min(2, 33, 44, 55, 66); //min
matVal = Math.max(-22, 33, 44, 55, 66); //max
console.log("contingut matVal:", matVal);


console.log("[CONCAT AND APPENDING]:");
const firstName = "Ivan";
const lastName = "Berenguer";
let nomConc;
nomConc = firstName + ' I\'m ' + lastName;
console.log("contingut nomConc:", nomConc);
let nomAppend;
nomAppend = firstName;
nomAppend += lastName;
console.log("contingut nomAppend:", nomAppend);


console.log("[INTERPOLATION]:");
let nomInterp;
nomInterp = `My name is ${firstName} ${lastName} `;
console.log("contingut nomInterp:", nomInterp);


console.log("[COMPARISON OPERATORS]:");
const x = 10;
const y = 20;
console.log("equal x==y:", x == y);
console.log("strict equal x===y:", x === y); //fins hi tot compara el dataType
console.log("not equal x!=y:", x != y);
console.log("strict not equal x!==y:", x !== y);
console.log("greater then x>y:", x > y);
console.log("greater then or eq x>=y:", x >= y);
console.log("less then x<y:", x < y);
console.log("less then or eq x<=y:", x <= y);



console.log("[LOGICAL OPERATORS]:");
if (x > 21 && y < 40 || 1 == 2) { //&& = and, || = or
    console.log('logic True');
} else {
    console.log('logic False');
}
const z = x ? 'ternary1 True' : 'ternary1 False';
console.log('z:', z);
if (!y) { //
    console.log('ternary2 True');
} else {
    console.log('ternary2 False');
}

console.log("[ASSIGNMENT OPERATORS]:");
let aoVal = 15;
aoVal += 5; //== 15+5
aoVal -= 5; //== 20-5
aoVal *= 2; //== 15*2
aoVal /= 2; //== 30/2
aoVal %= 5; //== 15 mod 2
console.log("aoVal:", aoVal);


console.log("[ARRAY AND ARRAY METHODS]:");
//Bracket method
const arrm1 = ["Software dev", "HR Analyst", "Marketing", "Finance"];
//Array constructor
const arrm2 = new Array(22, 25, 55, 74, 44, 88);
//Mix datatypes
const arrm3 = [24, 'Ivan', "Avinyo", undefined, true];
//Mutate to show different things
let arrm4;
arrm4 = arrm2.length; //conta numero de valors, comença per 1
arrm4 = Array.isArray(arrm2); //Check if is an Array
arrm4 = arrm1["Marketing"]; //=undefined, Not working
arrm4 = arrm1[2]; //Per index, comença per 0
arrm1.push("Data analyst"); //Afegir un valor al final del array
arrm1.pop(); //Eliminar un valor del final del array
arrm4 = arrm1.shift(); //Eliminar un valor del principi del array
arrm4 = arrm1.unshift("Software dev"); //Insertar un valor al inici del array
arrm4 = arrm1.join(";"); //Els enganxa amb comma, a menys de que especifiquem un altre glue
arrm4 = arrm1.reverse();//Reverse, els canvis són permanents (fets al origen)
arrm4 = arrm1.sort(); //Ordenar asc
console.log("arrm4:", arrm4);
console.log("arrm1:", arrm1);


console.log("[DATE AND TIME]:");
//els objects estan en el timezone del client (navegador)
const now = new Date;
console.log("now:", now);
console.log("typeof now:", typeof now); //Object
let nowVal = now.toString();
console.log("nowVal:", nowVal);
console.log("typeof nowVal:", typeof nowVal); //String

let birthday = new Date(1985, 5, 11, 20, 30, 05); //11-06-1985, JS conta els mesos desde 0 fins 11
//  Es pot complementar amb 3 parametres opcionals adicionals al final: hores, minuts i segons
birthday = new Date('june 11 1985 20:30:05'); //Igual que a dalt
birthday = new Date('06/11/1985 20:30:05');  //Igual que a dalt
let xbirthday = birthday.getMonth(); //0-11: typeof number
xbirthday = birthday.getDay(); //0-6 
console.log("birthday:", birthday);
console.log("typeof birthday:", typeof birthday);


console.log("[CONDITIONALS]:");
/*
if //si l'IF es true
else if //Nomes si l'IF es  false
else //Nomes si l'IF i tots els else if són falsos
switch //multiples opcions de la variable, com en PHP
*/
let bool = true;
if (bool) { //=== strict (boolean comparison), == equal (tots)
    console.log('this sitatement is true');
} else if (!bool) { //!== not strict (boolean comparison), != not equal (tots)
    console.log('this sitatement is false');
} else {
    //no podria pasar amb la combinacio if else if superior. en aquest cas seria igual al !bool
    console.log('else invoked');
}

console.log("[SWITCH]:");
let swValue = "5";
switch (swValue) {
    case 1:
        console.log('swValue == 1');
        break;
    case (2):
        console.log('swValue == 2');
    case '5':
        console.log('swValue == \'5\'');
        break;
    default:
        console.log('swValue not in [1-2] and \'5\' (str) ');
}




console.log("[LOOPS AND ITERATIONS]:");
//While loop
let wl = 0;
while (wl < 5) {
    console.log('while wl:', wl);
    wl++;
}
//do-while loop
do {
    console.log('do-loop wl:', wl);
    wl++;
} while (wl < 10);
//for loop
for (let wl = 0; wl < 5; wl++) {
    console.log('for wl:', wl);
}
//for-in loop (en objects)
let objLoop = {
    name: "Ivan",
    age: 36,
    job: "Web developer",
    City: "Avinyó"
};
for (let x in objLoop) {
    console.log('for-in x: ' + x + ' is ' + objLoop[x]);
}
//for-of loop (en array)
let arrLoop = ['BMV', 'Volvo', 'Audi'];
for (let x in arrLoop) {
    console.log('for-of x: ' + x + ' is ' + arrLoop[x]);
}


console.log("[FUNCTIONS]:");
//Use camelCase
function showMessage() {
    console.log('This is inside function "showMessage" ', showMessage);
}
showMessage();
function showMessageWithParams(param1, param2 = "lalala") {
    console.log('This is inside function "showMessageWithParams"; \nparam1:"' + param1 + '", \nparam2="' + param2 + '"', showMessageWithParams);
}
showMessageWithParams("wii");
function addNumbers(num1, num2) {
    return num1 + num2;
}
console.log(addNumbers + ":", addNumbers(1, 2));
console.log(addNumbers + ":", addNumbers(1, 2));



console.log("[WINDOW OBJECT]:");

window.alert("Això és un window.alert");

const prompt1 = window.prompt("Això és un window.prompt, entra algo");
alert("has entrat: \n" + prompt1);

if (window.confirm("El negre en anglès és 'black' ?")) {
    console.log("En el confirm: Has dit que si");
} else {
    console.log("En el confirm: Has dit que no");
}
let ow = window.outerWidth;
let oh = window.outerHeight;
console.log("outerWidth:" + ow + " outerHeight:" + oh + " ");

let iw = window.innerWidth;
let ih = window.innerHeight;
console.log("innerWidth:" + iw + " innerHeight:" + ih + " ");

console.log("window-location props: ",window.location);
let varsGet = window.location.search;
console.log("window-navigator props: ",window.navigator);


